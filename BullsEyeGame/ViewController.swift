//
//  ViewController.swift
//  BullsEyeGame
//
//  Created by Максим Семений on 5/21/19.
//  Copyright © 2019 Максим Семений. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        startNewRound()
        
        let thumbImageNormal = #imageLiteral(resourceName: "SliderThumb-Normal")
        slider.setThumbImage(thumbImageNormal, for: .normal)
        
        let thumbImageHighlighted = #imageLiteral(resourceName: "SliderThumb-Highlighted")
        slider.setThumbImage(thumbImageHighlighted, for: .highlighted)
        
        let insets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
        
        let trackLeftImage = #imageLiteral(resourceName: "SliderTrackLeft")
        let trackLeftImageResizable = trackLeftImage.resizableImage(withCapInsets: insets)
        slider.setMinimumTrackImage(trackLeftImageResizable, for: .normal)
        
        let trackRightImage = #imageLiteral(resourceName: "SliderTrackRight")
        let trackRightImageResizable = trackRightImage.resizableImage(withCapInsets: insets)
        slider.setMaximumTrackImage(trackRightImageResizable, for: .normal)
    }
    
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var slider : UISlider!
    @IBOutlet weak var targetScore : UILabel!
    @IBOutlet weak var scoreLabel : UILabel!
    @IBOutlet weak var roundLabel : UILabel!
    @IBOutlet weak var startOverButton : UIButton!
    
    var currentValue = 0
    var targetValue = 0
    var score = 0
    var round = 0
    
    @IBAction func showAllert() {
        
        let difference = abs(targetValue - currentValue)
        var points = 100 - difference
        score += points
        
        let title : String
        
        if difference == 0 {
            points += 100
            title = "Perfect!"
        } else if difference < 5 {
            title = "You almost had it!"
            if difference == 1 {
                points += 50
            }
        } else if difference < 10 {
            title = "Pretty good!"
        } else {
            title = "Not even close"
        }
        
        
        let message = "You scored \(points) points"
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Awesome", style: .default, handler: {
            action in
            self.startNewRound()
        })
        
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func sliderMoved(_ slider : UISlider) {
        let rounded = slider.value.rounded()
        currentValue = Int(rounded)
    }
    @IBAction func roundReset(_ sender: AnyObject) {
        score = 0
        round = 0
        startNewRound()
    }
    
    
    func startNewRound () {
        round = round + 1
        targetValue = Int.random(in: 1...100)
        currentValue = 50
        slider.value = Float(currentValue)
        updateTargetScore()
    }
    
    func updateTargetScore () {
        targetScore.text = String(targetValue)
        scoreLabel.text = String (score)
        roundLabel.text = String(round)
    }
    
}

