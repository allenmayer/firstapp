//
//  AboutAuthorView.swift
//  BullsEyeGame
//
//  Created by Максим Семений on 5/29/19.
//  Copyright © 2019 Максим Семений. All rights reserved.
//

import UIKit

class AboutAuthorView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        allenMayer.image = UIImage(named: "Allen")
    }
    
    @IBOutlet weak var allenMayer: UIImageView!
    
    @IBAction func goBack() {
        dismiss(animated: true, completion: nil)
    }
}
